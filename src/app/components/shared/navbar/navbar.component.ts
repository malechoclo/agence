import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isOpen:boolean=false;
  constructor() { }

  ngOnInit() {
  }
  toggleMenu(){
    console.log("click");

    this.isOpen = !this.isOpen;
    console.log(this.isOpen);
  }

}

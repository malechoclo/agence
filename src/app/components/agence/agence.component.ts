import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup ,ReactiveFormsModule } from '@angular/forms';

import { UsuariosService } from '../../services/usuarios.service';
import { DataService } from '../../services/data.service';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-agence',
  templateUrl: './agence.component.html',
  styleUrls: ['./agence.component.scss']
})
export class AgenceComponent implements OnInit {
  addUserForm :FormGroup;
  isCargando:boolean = false;
  isRelatorio:boolean = false;
  isChart:boolean = false;
  isPizza:boolean = false;
  isPizzaSinData    = false;
  isChartSinData     = false;

  dropUsuariosArray:any = [];
  selectedValueUsuarios:any;

  allSelectedUsers:any = [];
  allSelectedUsersAux:any = [];

  fechaInicio:any = new Date('01/01/2007');
  fechaTermino:any = new Date();;
  iFormated:any;
  tFormated:any;

  pickerDesde:any;
  pickerHasta:any ;
  totalLiquido:any = 0;

  relatorioArray:any = [];

  showErrorUsuarios:boolean=false;
  showErrorFechas:boolean=false;


  /* DECLARACIONES PARA GRÁFICOS BARRAS */

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public barChartLabels:string[] = [];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;

  public barChartData:any[] = [];

  /* FIN DECLARACIONES PARA GRÁFICOS BARRAS */

  /* DECLARACIONES PARA GRÁFICOS PIZA */


  public pieChartLabels:string[] = [];
  public pieChartData:number[] = [];
  public pieChartType:string = 'pie';


  /* FIN DECLARACIONES PARA GRÁFICOS PIZA */

  constructor(private u:UsuariosService,private data:DataService) {
    this.addUserForm = new FormGroup ({
      'usuario': new FormControl()
    });
  }

  ngOnInit() {
    this.u.getConsultores().subscribe(data=>{
      this.dropUsuariosArray = data.data;
    });
  }

  getErroClass(v){
    if(parseInt(v)<0){
      return 'error';
    }
  }

  addUser(){
    console.log(this.selectedValueUsuarios);
    let i = this.allSelectedUsers.indexOf(this.selectedValueUsuarios);
    if(i<0){
      this.allSelectedUsers.push(this.selectedValueUsuarios);
    }
  }

  removeUserFromList(u){
    let i = this.allSelectedUsers.indexOf(u);
    if(i>=0){
      delete this.allSelectedUsers[i];
    }
  }

  addFecha(){
    console.log(this.fechaInicio);
    console.log(this.fechaTermino);
  }

  getRelatorio(){
    this.isChartSinData=false;
    this.isPizzaSinData=false;
    this.isRelatorio=false;
    if(!this.processDataForm()){
      return;
    }
    this.isCargando = true;

    this.isChart=false;
    this.isPizza=false;
    this.isRelatorio=true;

    this.data.getRelatorio(this.iFormated,this.tFormated,this.allSelectedUsers).subscribe(data=>{
      console.log("relatiro data",data);
      this.relatorioArray = data.data;
      this.isCargando = false;
    });
  }

  getGrafico(){
    this.isChart=false;
    this.isChartSinData=false;
    this.isPizza=false;
    this.isRelatorio=false;
    if(!this.processDataForm()){
      return;
    }

    this.isCargando = true;

    this.data.getGrafico(this.iFormated,this.tFormated,this.allSelectedUsers).subscribe(data=>{
      console.log("relatiro data for grafico",data.data);
      let dataByPeriodo = [];
      let brutoArray = [];
      this.barChartData=[];
      console.log(data.data[0].sindata);
      if(data.data[0].sindata){
        this.isChartSinData = true;
        this.isCargando =false;
        this.isChart=true;
        return;
      }

      if(data.data[0].fechas){
        this.barChartLabels = data.data[0].fechas;
      }

      if(data.data[0].fechas){

        for(let l in data.data[0].label){
          this.barChartData.push({data:data.data[0].data[l] , label:data.data[0].label[l]}) ;
        }

        /* SE AGREGA UNA COLUMNA CON EL BRUTO PROMEDIO*/
        for(var i=0;i<= data.data[0].data[0].length ;i++){
          brutoArray.push(data.data[0].bruto);
        }
        this.barChartData.push({data:brutoArray , label:'PROMEDIO'}) ;
      }

      this.isCargando = false;
      this.isChart=true;
    });

    console.log(this.allSelectedUsers);
  }

  getPizza(){
    this.isPizzaSinData=false;
    this.isCargando =true;
    this.isChart=false;
    this.isPizza=false;
    this.isRelatorio=false;
    if(!this.processDataForm()){
      return;
    }
    this.pieChartLabels = [];
    this.pieChartData= [];
    this.data.getPizza(this.iFormated,this.tFormated,this.allSelectedUsers).subscribe(data=>{
      console.log("relatiro data for pizza",data.data);
      console.log(data.data[0].labels);
      console.log(data.data[0].bruto);

      if(data.data[0].sindata){
        this.isPizzaSinData =true;
        this.isCargando =false;
        this.isPizza=true;
        return;
      }
      this.pieChartLabels=data.data[0].labels;
      this.pieChartData=data.data[0].bruto;
      this.isPizza=true;
      this.isRelatorio=false;

    });

    console.log(this.allSelectedUsers);
  }

  processDataForm(){
    var valido = true;
    let inicio =new Date(this.fechaInicio);
    let termino =new Date(this.fechaTermino);
    let auxUsers = [];
    this.showErrorUsuarios    = false;
    this.showErrorFechas       = false;
    this.isCargando        = false;
    this.iFormated = inicio.getFullYear()+'-'+(inicio.getMonth()+1)+'-'+inicio.getDay();
    this.tFormated = termino.getFullYear()+'-'+(termino.getMonth()+1)+'-'+termino.getDay();

    for(let u in this.allSelectedUsers){
      if(typeof this.allSelectedUsers[u] !== null){
        auxUsers.push(this.allSelectedUsers[u]);
      }
    }

    if(auxUsers.length==0){
      this.showErrorUsuarios = true;
      valido = false;
    };

    if(typeof this.fechaInicio === 'undefined'|| typeof this.fechaTermino === 'undefined' ){
      this.showErrorFechas = true;
      valido = false;
    }
    console.log(valido);
    return valido;
  }

}

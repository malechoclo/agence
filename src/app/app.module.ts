import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

//ROUTING
import {APP_ROUTING} from './app.routes'
import {GlobalVariables} from './app.globals'

/* COMPONENTS */

import { NotfoundComponent } from './components/shared/notfound/notfound.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { AgenceComponent } from './components/agence/agence.component';

/* SERVICES */
import { UsuariosService } from './services/usuarios.service';
import { DataService } from './services/data.service';

/* UTILITIES */
import { MaterialModule } from './modules/material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    NotfoundComponent,
    AgenceComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    APP_ROUTING,
    GlobalVariables,
    ChartsModule
  ],
  providers: [
    UsuariosService,
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

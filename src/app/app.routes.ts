import { RouterModule, Routes } from '@angular/router';

import { AgenceComponent } from './components/agence/agence.component';
import { NotfoundComponent } from './components/shared/notfound/notfound.component';


const APP_ROUTES: Routes = [
  { path: '', component: AgenceComponent },
  { path: 'agence', component: AgenceComponent },
  { path: 'proyectos', component: NotfoundComponent },
  { path: 'admin', component: NotfoundComponent },
  { path: 'comercial', component: NotfoundComponent },
  { path: 'financiero', component: NotfoundComponent },
  { path: 'perfil', component: NotfoundComponent },
  { path: '**', pathMatch: 'full', redirectTo: '' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);

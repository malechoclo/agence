import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';
import {GlobalVariables} from '../app.globals'
import 'rxjs/Rx';

@Injectable()
export class DataService {

  constructor(private globals:GlobalVariables,private http:Http) {

  }

  getRelatorio(inicio,termino,usuarios):any{

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let data = {
      fecha_inicio:inicio,
      fecha_termino:termino,
      usuarios:usuarios
    }

    let ws =`${this.globals.ws}/relatorio`
    return this.http.post(ws,data,{headers})
    .map(data =>{
      return data.json();
    });
  }

  getGrafico(inicio,termino,usuarios):any{
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let data = {
      fecha_inicio:inicio,
      fecha_termino:termino,
      usuarios:usuarios
    }

    let ws =`${this.globals.ws}/grafico`
    return this.http.post(ws,data,{headers})
    .map(data =>{
      return data.json();
    });
  }

  getPizza(inicio,termino,usuarios):any{
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let data = {
      fecha_inicio:inicio,
      fecha_termino:termino,
      usuarios:usuarios
    }

    let ws =`${this.globals.ws}/pizza`
    return this.http.post(ws,data,{headers})
    .map(data =>{
      return data.json();
    });
  }
}

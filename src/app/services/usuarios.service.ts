import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';
import {GlobalVariables} from '../app.globals'
import 'rxjs/Rx';
@Injectable()
export class UsuariosService {

  constructor(private globals:GlobalVariables,private http:Http) {

  }

  getClientes():any{
    console.log(this.globals.ws);
    return this.http.get(`${this.globals.ws}/clientes`)
    .map(res=>{
      console.log("clientes",res.json());
    })
  }

  getConsultores():any{
    console.log(`${this.globals.ws}/consultores`);
    return this.http.get(`${this.globals.ws}/consultores`)
    .map(res=>{
      console.log(res.json());
      return res.json();
    })
  }


}
